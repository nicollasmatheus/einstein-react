import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Navbar from './components/Navbar'
import Users from './components/Users'
import UserForm from './components/Users/UserForm'

export default function App() {
  return (
    <Router>
      <div>
        <Navbar />
        <Routes>
          <Route path='/' element={<Users />} />
          <Route path='/create' element={<UserForm />} />
          <Route path='/edit' element={<UserForm />} />
        </Routes>
      </div>
    </Router>
  );
}
