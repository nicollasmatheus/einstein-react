import { useState, useEffect } from "react";
import { useNavigate, useLocation } from "react-router-dom";

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import { UserService } from "../../apis/services/users.service";
import { IUser } from '../../apis/types/user'
import { userFormStyles } from "./Users.styles";
import Loading from "../Loading";

export default function UserCreate() {
  const classes = userFormStyles();
  const navigate = useNavigate();
  const { state: location } = useLocation()

  const [error, setError] = useState<{ status: boolean, message: string | undefined | null} | null>({ status: false, message: null })

  const [{
    name, email, gender, age
  }, setUser] = useState({
    name: '',
    email: '',
    gender: '',
    age: 0
  })

  const [loading, setLoading] = useState(false)

  useEffect(() => {
    if(location?.user) {
      setUserState(location.user)
    }
  }, [location])

  const setUserState = (arg: IUser) => setUser(prevUser => ({ ...prevUser, ...arg }))

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    setLoading(true)

    const data = {
      name,
      email,
      gender,
      age: parseInt(age.toString()),
    }

    try {
      console.log('location', location)
      if (location?.user) {
        await UserService.put(data as unknown as IUser, location.user._id)
      } else {
        await UserService.post(data as unknown as IUser)
      }
      if(error?.status) setError(null)
    } catch (e: any) {
      setError({ status: true, message: e.toString() })
    }

    setLoading(false)

    return navigate("/");
  }

  return (
    <Container maxWidth="xs">
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          {location?.user ? 'Editar Usuário' : 'Criar Usuário'}
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                name="name"
                variant="outlined"
                required
                fullWidth
                id="name"
                label="Nome"
                value={name}
                onChange={(e) => setUserState({ name: e.target.value } as IUser)}
                autoFocus
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                value={email}
                label="Email"
                onChange={(e) => setUserState({ email: e.target.value } as IUser)}
              />
            </Grid>
             <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                value={age}
                id="age"
                label="Idade"
                onChange={(e) => setUserState({ age: e.target.value } as unknown as IUser)}
              />
            </Grid>
             <Grid item xs={12} sm={6}>
              <Select
                labelId="demo-simple-select-label"
                variant="outlined"
                required
                value={gender}
                fullWidth
                name="genre"
                label="Sexo"
                onChange={(e) => setUserState({ gender: e.target.value } as unknown as IUser)}
              >
                <MenuItem value={'masculino'}>Masculino</MenuItem>
                <MenuItem value={'feminino'}>Feminino</MenuItem>
              </Select>
            </Grid>
          </Grid>
          {loading ? <Loading /> : (
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Salvar
            </Button>
          )}
          {error && <span>{error.message}</span>}
        </form>
      </div>
    </Container>
  );
}
