import { useEffect, useState } from "react";

import toast, { Toaster } from 'react-hot-toast';
import { useNavigate, Link } from "react-router-dom";

import { UserService } from "../../apis/services/users.service";
import { IUser } from "../../apis/types/user";
import { usersTableStyle } from "./Users.styles";
import Loading from "../Loading";

import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ButtonGroup from '@material-ui/core/ButtonGroup';

export default function UserTable() {
  const classes = usersTableStyle();
  const navigate = useNavigate()

  const [users, setUsers] = useState<IUser[]>([]);
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    getAllUsers()
  }, [])

  const getAllUsers = async () => {
   try {
    const res = await UserService.getAll()
    setUsers(res)
   } catch (e) {
    toast.error('Something wrong happened!')
   }
  }

const onDeleteUser = async (id: string) => {
  try {
    await UserService.delete(id)
    toast.success('User deleted!')
    return getAllUsers()
  } catch (e) {
    toast.error('Something wrong happened!')
  }
}

const onEditUser = (user: IUser) => navigate("/edit", {
  state: { user }
})

if (loading) {
  return (
    <Loading />
  )
}

  return (
    <div className={classes.root}>
      <Container className={classes.container} maxWidth="lg">
        <Paper className={classes.paper}>
          <Box display="flex" className={classes.tableHeader}>
            <Box>
              <Typography component="h2" variant="h6" color="primary" gutterBottom>
                Lista de Usuários
              </Typography>
            </Box>
            <Box>
              <Link to="/create">
                <Button variant="contained" color="primary">
                  Adicionar
                </Button>
              </Link>
            </Box>
          </Box>
          <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="left">Nome</TableCell>
                <TableCell align="left">Email</TableCell>
                <TableCell align="left">Idade</TableCell>
                <TableCell align="center">Sexo</TableCell>
                <TableCell align="center">Ação</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {users.map((user) => (
                <TableRow key={user._id}>
                  <TableCell align="left">{user.name}</TableCell>
                  <TableCell align="left">{user.email}</TableCell>
                  <TableCell align="left">{user.age}</TableCell>
                  <TableCell align="left">{user.gender}</TableCell>
                  <TableCell align="center">
                    <ButtonGroup color="primary" aria-label="outlined primary button group">
                      <Button onClick={() => onEditUser(user)}>Editar</Button>
                      <Button onClick={() => onDeleteUser(user._id)}>Excluir</Button>
                    </ButtonGroup>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        </Paper>
      </Container>
      <Toaster />
    </div>
  );
}
