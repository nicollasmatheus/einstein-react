import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Link } from "react-router-dom";
import { useStyles } from "./Navbar.styles";

export default function Navbar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Link className={classes.navlink} to="/">
            <Typography variant="h6" className={classes.title}>
              Einstein CRUD
            </Typography>
          </Link>
        </Toolbar>
      </AppBar>
    </div>
  );
}
