import CircularProgress from '@material-ui/core/CircularProgress';
import { useLoadingStyles } from './Loading.styles';

export default function Loading() {
  const classes = useLoadingStyles()

  return (
    <div className={classes.loading}>
      <CircularProgress />
    </div>
  );
}
