export interface IUser {
  _id: string
  name: string;
  age: number;
  email: string;
  gender: string;
}
