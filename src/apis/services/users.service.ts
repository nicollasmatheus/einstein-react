import axios from "axios"
import { IUser } from "../types/user"

const DEFAULT_URL = 'http://localhost:3000/user'

export const UserService = {
  get: async function (id: string) {
    const response = await axios.get(`${DEFAULT_URL}/${id}`)
    return response
  },

  getAll: async function () {
    const response = await axios.get(DEFAULT_URL)
    return response.data.userData as unknown as IUser[]
  },

  post: async function (data: IUser) {
    const response = await axios.post(DEFAULT_URL, data)
    return response
  },

   put: async function (data: IUser, id: string) {
    const response = await axios.put(`${DEFAULT_URL}/${id}`, data)
    return response
  },

  delete: async function (id: string) {
    const response = await axios.delete(`${DEFAULT_URL}/${id}`)
    return response
  },
}
