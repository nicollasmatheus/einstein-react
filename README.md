# What is this

This is Create React App Project with typescript

## Installation

```bash
yarn install
```

## Usage

```bash
yarn start
```

This project should run in a port different that port:3000 because that's the port of the api

## License

[MIT](https://choosealicense.com/licenses/mit/)
